# Getting Started With Schematics


```
npx @angular-devkit/schematics-cli blank --name=schematics
```

Initialize a new Angular workspace with a single application having jest/prettier/eslint preconfigured:

```
npm init -y
npx --package  @kim-ho-lan/schematics@0.0.16 --package @angular/cli ng new -c @kim-ho-lan/schematics
```


