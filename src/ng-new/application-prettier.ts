import {chain, Rule, SchematicContext, Tree} from "@angular-devkit/schematics";
import {join, normalize} from '@angular-devkit/core';
import {RunSchematicTask} from "@angular-devkit/schematics/tasks";

export function applicationPrettier(options: any): Rule {
    return (_: Tree, context: SchematicContext) => {
        context.addTask(new RunSchematicTask('application__application_prettier', options), []);
    };
}

export function applicationPrettierFactory({workspace}: any): Rule {
    const options = {
        workspace,
    }
    return chain([
        prettierIgnore(options),
        prettierRc(options),
    ])
}

function prettierIgnore({workspace}: any): Rule {
    return (tree: Tree) => {
        const prettierIgnore = `\
node_modules
polyfills.ts
*.js
`;
        const path = join(normalize(`${workspace}`), '.prettierignore')

        if (!tree.exists(path)) {
            tree.create(path, prettierIgnore);
            return tree;
        }
        tree.overwrite(path, prettierIgnore);
        return tree;
    };
}

function prettierRc({workspace}: any): Rule {
    return (tree: Tree) => {
        const prettierRcJson = {
            "singleQuote": true,
            "trailingComma": "es5",
            "endOfLine": "auto",
            "printWidth": 120,
            "tabWidth": 2,
            "bracketSpacing": true,
            "arrowParens": "always"
        };
        const path = join(normalize(`${workspace}`), '.prettierrc')

        if (!tree.exists(path)) {
            tree.create(path, serializeJson(prettierRcJson));
            return tree;
        }
        tree.overwrite(path, serializeJson(prettierRcJson));
        return tree;
    };
}


function serializeJson(json: unknown): string {
    return `${JSON.stringify(json, null, 2)}\n`;
}
