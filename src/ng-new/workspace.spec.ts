import {Tree} from '@angular-devkit/schematics';
import {SchematicTestRunner} from '@angular-devkit/schematics/testing';
import * as path from 'path';

const collectionPath = path.join(__dirname, '../collection.json');

describe('schematics', () => {
    const options = {workspace: '@name@'} as const;
    const runner = new SchematicTestRunner('schematics', collectionPath);

    it('works', async () => {
        const tree = await runner
            .runSchematicAsync('ng-new', options, Tree.empty())
            .toPromise();

        expect(tree.files.sort()).toEqual([
            '/@name@/.editorconfig',
            '/@name@/.gitignore',
            '/@name@/.vscode/extensions.json',
            '/@name@/.vscode/launch.json',
            '/@name@/.vscode/tasks.json',
            '/@name@/README.md',
            '/@name@/angular.json',
            '/@name@/package.json',
            '/@name@/tsconfig.json',
        ]);
    });

});
