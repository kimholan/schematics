import {chain, externalSchematic, Rule, SchematicContext, Tree} from "@angular-devkit/schematics";
import {RunSchematicTask} from "@angular-devkit/schematics/tasks";
import {Observable} from "rxjs";
import {spawn} from "child_process";

export function workspaceFinalize(options: any): Rule {
    return (_: Tree, _context: SchematicContext) => {
        let finalizeTaskId = _context.addTask(new RunSchematicTask('ng-new__workspace_finalize', options), []);
        _context.addTask(new RunSchematicTask('ng-new__workspace_finalize_post', options), [finalizeTaskId]);
    }
}

export function workspaceFinalizeFactory(options: any): Rule {
    return chain([
        ngAddEsLintSchematics(options),
        workspaceCleanup(options),
    ]);
}

export function workspaceFinalizePostFactory(options: any): Rule {
    const {workspace} = options;

    return chain([
        spawnCommand({command: 'npm', args: ['install', '--prune'], workspace}),
        spawnCommand({command: 'ng', args: ['lint', '--fix'], workspace}),
    ]);
}

function ngAddEsLintSchematics({workspace}: any): Rule {
    return externalSchematic('@angular-eslint/schematics', 'ng-add', {}, {scope: workspace});
}

function workspaceCleanup({workspace}: any): Rule {
    return (tree: Tree, context: SchematicContext) => {
        tree.delete(`${workspace}/test-config.helper.ts`);
        tree.delete(`${workspace}/.editorconfig`)
        tree.delete(`${workspace}/.vscode/extensions.json`)
        tree.delete(`${workspace}/.vscode/launch.json`)
        tree.delete(`${workspace}/.vscode/tasks.json`)
        tree.delete(`${workspace}/.vscode`)
        context.logger.log('info', `✅️ Deleted redundant files`);
    };
}

function spawnCommand({command, args, workspace}: { command: string; args: string[]; workspace: string }): Rule {
    return (host: Tree) => {
        return new Observable<Tree>(subscriber => {
            const child = spawn(command, args, {stdio: 'inherit', cwd: workspace});
            child.on('error', error => {
                subscriber.error(error);
            });
            child.on('close', () => {
                subscriber.next(host);
                subscriber.complete();
            });
            return () => {
                child.kill();
            };
        });
    };
}
