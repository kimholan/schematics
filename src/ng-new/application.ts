import {chain, externalSchematic, Rule, SchematicContext, Tree} from '@angular-devkit/schematics';
import {RunSchematicTask} from "@angular-devkit/schematics/tasks";
import {applicationFinalize} from "./application-finalize";
import {applicationEslint} from "./application-eslint";
import {applicationPrettier} from "./application-prettier";

export function application(options: any): Rule {
    return (_: Tree, context: SchematicContext) => {
        context.addTask(new RunSchematicTask('application', options), []);
    };
}

export function applicationFactory(options: any): Rule {
    return chain([
        angularCreateApplication(options),
        addEsLintToProject(options),
        applicationEslint(options),
        applicationPrettier(options),
        applicationFinalize(options),
    ]);
}

function angularCreateApplication({workspace, application, ...defaults}: any): Rule {
    const options = {...defaults, name: application}

    return externalSchematic('@schematics/angular', 'application', options, {
        scope: workspace
    })
}

function addEsLintToProject({workspace, application}: any): Rule {
    const options = {project: application}

    return externalSchematic('@angular-eslint/schematics', 'add-eslint-to-project', options, {
        scope: workspace
    })
}

