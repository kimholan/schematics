import {Rule, SchematicContext, Tree} from "@angular-devkit/schematics";
import {addPackageJsonDependency, NodeDependency, NodeDependencyType} from "@schematics/angular/utility/dependencies";

export function workspaceDependencies({workspace}: any): Rule {
    return (tree: Tree, context: SchematicContext) => {
        [
            dependencyDev('@angular-eslint/schematics', '^13.1.0'),
            dependencyDev('prettier', '^2.6.2'),
            dependencyDev('eslint-config-prettier', '^8.5.0'),
            dependencyDev('@typescript-eslint/eslint-plugin', '^5.18.0'),
            dependencyDev('@typescript-eslint/parser', '^5.18.0'),
            dependencyDev('@typescript-eslint/typescript-estree', '^5.18.0'),
            dependencyDev('eslint-plugin-prettier', '^4.0.0'),
            dependencyDev('eslint-plugin-es', '^4.1.0'),
            dependencyDev('eslint-plugin-no-null', '^1.0.2'),
            dependencyDev('eslint-plugin-prettier', '4.0.0'),
        ].forEach(it => {
            addPackageJsonDependency(tree, it, `${workspace}/package.json`);
            context.logger.log('info', `✅️ Added "${it.name}" into ${it.type}`);
        });

        return tree;
    };
}


function dependencyDev(name: string, version: string, overwrite: boolean = true): NodeDependency {
    return dependency(name, version, NodeDependencyType.Dev, overwrite);
}

function dependency(name: string, version: string, type: NodeDependencyType = NodeDependencyType.Default, overwrite: boolean = true): NodeDependency {
    return {
        name,
        version,
        type,
        overwrite,
    }
}
