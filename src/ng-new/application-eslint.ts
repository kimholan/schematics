import {chain, Rule, SchematicContext, Tree} from "@angular-devkit/schematics";
import {join, normalize} from '@angular-devkit/core';
import {RunSchematicTask} from "@angular-devkit/schematics/tasks";

export function applicationEslint(options: any): Rule {
    return (_: Tree, context: SchematicContext) => {
        context.addTask(new RunSchematicTask('application__application_eslint', options), []);
    };
}

export function applicationEslintFactory({workspace, application}: any): Rule {
    const options = {
        workspace,
        prefix: 'app',
        projectRoot: `projects/${application}`,
        rootPath: '/',
    }
    return chain([eslint(options)])
}

// copied/modified from : https://github.com/angular-eslint/angular-eslint/tree/master/packages/schematics/src/add-eslint-to-project
function eslint({prefix, projectRoot, rootPath, workspace}: any): Rule {
    return (tree: Tree) => {
        const eslintJson = createProjectESLintConfig(rootPath, projectRoot, prefix);
        const path = join(normalize(`${workspace}/${projectRoot}`), '.eslintrc.json')

        if (!tree.exists(path)) {
            tree.create(path, serializeJson(eslintJson));
            return tree;
        }
        tree.overwrite(path, serializeJson(eslintJson));
        return tree;
    };
}

function createProjectESLintConfig(rootPath: string, projectRoot: string, prefix: string) {
    return {
        extends: `${offsetFromRoot(rootPath)}.eslintrc.json`,
        "ignorePatterns": [
            '!**/*',
            'main.ts'
        ],
        "overrides": [
            {
                "files": ["*.ts"],
                "plugins": [
                    "es",
                    "prettier",
                    "no-null"
                ],
                "excludedFiles": [],
                "parserOptions": {
                    "project": [
                        `${projectRoot}/tsconfig.app.json`,
                        `${projectRoot}/tsconfig.spec.json`
                    ],
                    "createDefaultProgram": true
                },
                "extends": [
                    "plugin:@angular-eslint/recommended",
                    "eslint:recommended",
                    "plugin:@typescript-eslint/recommended",
                    "plugin:@typescript-eslint/recommended-requiring-type-checking",
                    "plugin:@angular-eslint/template/process-inline-templates",
                    "plugin:prettier/recommended"
                ],
                "rules": {
                    "new-cap": [
                        "warn",
                        {
                            "capIsNewExceptions": [
                                "Inject",
                                "Action",
                                "Component",
                                "ContentChildren",
                                "ContentChild",
                                "Directive",
                                "Host",
                                "HostListener",
                                "Injectable",
                                "Input",
                                "NgModule",
                                "Optional",
                                "Output",
                                "Self",
                                "Select",
                                "Selector",
                                "State",
                                "ViewChild",
                                "ViewChildren"
                            ]
                        }
                    ],
                    "@typescript-eslint/no-explicit-any": [
                        "error"
                    ],
                    "es/no-object-assign": [
                        "error"
                    ],
                    "no-eq-null": [
                        "error"
                    ],
                    "no-unreachable-loop": [
                        "error"
                    ],
                    "no-console": [
                        "error"
                    ],
                    "no-unsafe-optional-chaining": [
                        "error"
                    ],
                    "no-promise-executor-return": [
                        "error"
                    ],
                    "no-useless-backreference": [
                        "error"
                    ],
                    "no-constructor-return": [
                        "error"
                    ],
                    "no-labels": [
                        "error"
                    ],
                    "no-new-wrappers": [
                        "error"
                    ],
                    "no-useless-concat": [
                        "error"
                    ],
                    "no-continue": [
                        "error"
                    ],
                    "no-new-object": [
                        "error"
                    ],
                    "consistent-return": [
                        "error"
                    ],
                    "@typescript-eslint/no-namespace": [
                        "off"
                    ],
                    "@typescript-eslint/unbound-method": [
                        "off"
                    ],
                    "@typescript-eslint/explicit-module-boundary-types": [
                        "error"
                    ],
                    "@angular-eslint/use-lifecycle-interface": [
                        "error"
                    ],
                    "@typescript-eslint/consistent-type-assertions": [
                        "error",
                        {
                            "assertionStyle": "as",
                            "objectLiteralTypeAssertions": "never"
                        }
                    ],
                    "@angular-eslint/directive-selector": [
                        "error",
                        {
                            "type": "attribute",
                            "prefix": prefix,
                            "style": "camelCase"
                        }
                    ],
                    "@angular-eslint/component-selector": [
                        "error",
                        {
                            "type": "element",
                            "prefix": prefix,
                            "style": "kebab-case"
                        }
                    ],
                    "no-null/no-null": 2
                }
            },
            {
                "files": ["*.html"],
                "extends": ["plugin:@angular-eslint/template/recommended"],
                "rules": {
                    "no-eq-null": [
                        "error"
                    ],
                    "@angular-eslint/template/no-negated-async": [
                        "warn"
                    ],
                    "@angular-eslint/template/eqeqeq": "error",
                    "no-null/no-null": 2
                }
            }
        ]
    };
}


function offsetFromRoot(fullPathToSourceDir: string): string {
    const parts = normalize(fullPathToSourceDir).split('/');
    let offset = '';
    for (let i = 0; i < parts.length; ++i) {
        offset += '../';
    }
    return offset;
}


function serializeJson(json: unknown): string {
    return `${JSON.stringify(json, null, 2)}\n`;
}
