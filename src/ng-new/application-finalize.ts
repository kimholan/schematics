import {chain, externalSchematic, Rule, SchematicContext, Tree} from '@angular-devkit/schematics';
import {RunSchematicTask} from "@angular-devkit/schematics/tasks";

export function applicationFinalize(options: any): Rule {
    return (_: Tree, context: SchematicContext) => {
        context.addTask(new RunSchematicTask('application__application_finalize', options), []);
    };
}

export function applicationFinalizeFactory(options: any): Rule {
    return chain([
        ngAddJestSchematic(options),
        applicationCleanup(options),
    ])
}

function ngAddJestSchematic({workspace}: any): Rule {
    return externalSchematic('@briebug/jest-schematic', 'ng-add', {}, {
        scope: workspace
    })
}

function applicationCleanup({workspace, application}: any): Rule {
    const projectDir = `${workspace}/projects/${application}`;
    return (tree: Tree) => {
        tree.delete(`${projectDir}/src/test.ts`);
        tree.delete(`${projectDir}/karma.conf.js`);
    };
}
