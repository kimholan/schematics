import {chain, externalSchematic, Rule} from '@angular-devkit/schematics';
import {workspaceDependencies} from "./workspace-dependencies";
import {workspaceFinalize} from "./workspace-finalize";
import {application} from "./application";

const ngDefaults = {
    strict: true,
    style: 'scss',
    inlineStyle: false,
    inlineTemplate: false,
} as const;

const ngNewDefaults = {
    ...ngDefaults,
    version: '13.3.2',
    createApplication: false,
} as const;

export function ngNew(options: any): Rule {
    return chain([
        angularNgNew(options),
        workspaceDependencies(options),
        workspaceFinalize(options),
        application({
            ...ngDefaults, ...options, routing: true,
        }),
    ]);
}

function angularNgNew({workspace}: any): Rule {
    return externalSchematic('@schematics/angular', 'ng-new', {
        ...ngNewDefaults,
        name: workspace,
        directory: workspace
    });
}
